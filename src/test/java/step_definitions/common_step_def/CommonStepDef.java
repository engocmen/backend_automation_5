package step_definitions.common_step_def;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static utils.Hooks.*;

public class CommonStepDef {

    private static Logger logger = LogManager.getLogger(CommonStepDef.class);

    int actualStatusCode;

    @Given("Validate that status code is {int}")
    public void validate_that_status_code_is(Integer expectedStatusCode) {

        logger.info("Getting the status code from the response");
        actualStatusCode = response.statusCode();

        assertThat(
                "Validating the status code",
                actualStatusCode,
                is(expectedStatusCode)
        );
    }

    @Then("I validate data from DB with API")
    public void iValidateDataFromDBWithAPI() throws IOException {

        for (int i = 0; i < queryResult.size(); i++) {
            Assert.assertEquals(String.valueOf(studentsMap.get(i).id), queryResult.get(i).get(0).toString());
            Assert.assertEquals(studentsMap.get(i).firstName, queryResult.get(i).get(3));
            Assert.assertEquals(studentsMap.get(i).lastName, queryResult.get(i).get(4));
            Assert.assertEquals(studentsMap.get(i).email, queryResult.get(i).get(2));
            Assert.assertEquals(studentsMap.get(i).dob, queryResult.get(i).get(1));
        }
    }

    @Then("I validate data between DB, API and UI")
    public void iValidateDataBetweenDBAPIAndUI() {


        for (int i = 0; i < studentsMap.size(); i++) {
            Assert.assertEquals(String.valueOf(studentsMap.get(i).id), queryResult.get(i).get(0).toString());
            Assert.assertEquals(studentsMap.get(i).firstName, queryResult.get(i).get(3));
            Assert.assertEquals(studentsMap.get(i).lastName, queryResult.get(i).get(4));
            Assert.assertEquals(studentsMap.get(i).email, queryResult.get(i).get(2));
            Assert.assertEquals(studentsMap.get(i).dob, queryResult.get(i).get(1));

            for (int j = 0; j < uiList.size(); j++) {
                if ((studentsMap.get(i).email).equals(uiList.get(j).get(1))) {

                    List<Object> firstAndLastNames = new ArrayList<>();
                    firstAndLastNames = List.of((uiList.get(j).get(0).toString().trim().split(" ")));

                    Assert.assertEquals(studentsMap.get(i).firstName, firstAndLastNames.get(0));
                    Assert.assertEquals(studentsMap.get(i).lastName, firstAndLastNames.get(1));
                    Assert.assertEquals(studentsMap.get(i).email, uiList.get(j).get(1));
                    Assert.assertEquals(studentsMap.get(i).dob, uiList.get(j).get(2));
                    break;

                }
            }

        }
    }
}
