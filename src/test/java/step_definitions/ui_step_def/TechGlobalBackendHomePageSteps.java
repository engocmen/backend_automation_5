package step_definitions.ui_step_def;

import com.github.javafaker.Faker;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import ui.pages.TechGlobalBackendTestingHomePage;
import utils.Driver;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static utils.Hooks.*;

public class TechGlobalBackendHomePageSteps {


    private static Logger logger = LogManager.getLogger(TechGlobalBackendHomePageSteps.class);

    WebDriver driver;
    TechGlobalBackendTestingHomePage techGlobalBackendTestingHomePage;
    Faker faker = new Faker();

    String email = faker.internet().emailAddress();

    @Before
    public void setup() {
        driver = Driver.getDriver();
        techGlobalBackendTestingHomePage = new TechGlobalBackendTestingHomePage();
    }


    @And("user enters {string}, {string}, email and {string} on backend page")
    public void userEntersEmailAndOnBackendPage(String firstName, String lastName, String dob) {

        techGlobalBackendTestingHomePage.firstName.sendKeys(firstName);
        techGlobalBackendTestingHomePage.lastName.sendKeys(lastName);
        techGlobalBackendTestingHomePage.backendEmail.sendKeys(email);
        techGlobalBackendTestingHomePage.dob.sendKeys(dob);

    }

    @When("user click on the add button")
    public void userClickOnTheAddButton() {
        techGlobalBackendTestingHomePage.add.click();
    }

    @And("page displays {string}")
    public void pageDisplays(String successMessage) {
        Assert.assertEquals(techGlobalBackendTestingHomePage.successMessage.getText(), successMessage);
    }

    @And("user can see all students on the page")
    public void userCanSeeWholeStudentsOnThePage() {

        studentsList = techGlobalBackendTestingHomePage.table;

        for (int i = 0; i < studentsList.size(); i++) {
            uiList.add(List.of(studentsList.get(i).getText().trim().split(" / ")));
        }

        System.out.println("\n List of the Students is below \n");

        System.out.println("\n" + uiList + "\n");

    }

    @Then("the system displays user's {string}, {string}, email and {string} on backend page")
    public void theSystemDisplaysUserSEmailAndOnBackendPage(String expectedFirstName,
                                                            String expectedLastName,
                                                            String expectedDob) throws ParseException {

        List<Object> firstAndLastNames = new ArrayList<>();
        int i;
        for (i = 0; i < uiList.size(); i++) {

          firstAndLastNames = List.of((uiList.get(i).get(0).toString().trim().split(" ")));

            // if you find the student in the list of students with the same email used when created a student
            if (uiList.get(i).get(1).equals(email)) {
                break;
            }
        }

        // removing replacing - with / to make the date similar with coming from API or DB
        expectedDob = expectedDob.trim().replaceAll("/", "-");

        // Formatting the date from input to backend
        DateFormat inputFormatter = new SimpleDateFormat("MM-dd-yyyy");
        Date date = (Date) inputFormatter.parse(expectedDob);
        DateFormat outputFormatter = new SimpleDateFormat("yyyy-MM-dd");
        expectedDob = outputFormatter.format(date);

        Assert.assertEquals(firstAndLastNames.get(0), expectedFirstName);
        Assert.assertEquals(firstAndLastNames.get(1), expectedLastName);
        Assert.assertEquals(uiList.get(i).get(1), email);
        Assert.assertEquals(uiList.get(i).get(2), expectedDob);

        System.out.println("Email added when creating a student " + email);
        System.out.println("Email in the student list " + uiList.get(i).get(1));
    }
}
