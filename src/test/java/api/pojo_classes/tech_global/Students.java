package api.pojo_classes.tech_global;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Students {

    /**
     * {
     *     "firstName": "{{$randomFirstName}}",
     *     "lastName": "{{$randomLastName}}",
     *     "email": "{{$randomEmail}}",
     *     "dob": "1992-12-15"
     * }
     */

    private String firstName;
    private String lastName;
    private String email;
    private String dob;
}
