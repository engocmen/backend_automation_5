package api;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;

public class APIAutomationSample {
    public static void main(String[] args) {


        /**
         * Response is an interface coming from the RestAssured Library
         * The Response variable "response" stores all the components of API calls
         * including the request and response
         * RestAssured is written with BDD flow
         *
         */
        Response response;

        Faker faker = new Faker();

        // Creating the post request
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer 5bd0864222b09a08d5a56ac71311588b8bbd59cee33fe96027ae65783fd4e0a8")
                .body("{\n" +
                        "    \"name\": \"" + faker.name().fullName() + "\",\n" +
                        "    \"gender\": \"male\",\n" +
                        "    \"email\": \"" + faker.internet().emailAddress() + "\",\n" +
                        "    \"status\": \"active\"\n" +
                        "}")
                .when().post("https://gorest.co.in/public/v2/users")
                .then().log().all().extract().response();

//        System.out.println(response.asString());

        int postId = response.jsonPath().getInt("id");

        System.out.println("Id is coming from response " + postId);

        // Creating the get request to fetch specific user
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer 5bd0864222b09a08d5a56ac71311588b8bbd59cee33fe96027ae65783fd4e0a8")
                .when().get("https://gorest.co.in/public/v2/users/" + postId)
                .then().log().all().extract().response();

//        response = RestAssured
//                .given().log().all()
//                .header("Content-Type", "application/json")
//                .header("Authorization", "Bearer 5bd0864222b09a08d5a56ac71311588b8bbd59cee33fe96027ae65783fd4e0a8")
//                .when().get("https://gorest.co.in/public/v2/users")
//                .then().log().all().extract().response();

        // Creating the PUT request to update the existing user
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer 5bd0864222b09a08d5a56ac71311588b8bbd59cee33fe96027ae65783fd4e0a8")
                .body("{\n" +
                        "    \"name\": \"" + faker.name().fullName() + "\",\n" +
                        "    \"gender\": \"male\",\n" +
                        "    \"email\": \"" + faker.internet().emailAddress() + "\",\n" +
                        "    \"status\": \"active\"\n" +
                        "}")
                .when().put("https://gorest.co.in/public/v2/users/" + postId)
                .then().log().all().extract().response();


        // Creating the get request to update the existing user
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer 5bd0864222b09a08d5a56ac71311588b8bbd59cee33fe96027ae65783fd4e0a8")
                .body("{\n" +
                        "    \"name\": \"" + faker.name().fullName() + "\",\n" +
                        "    \"gender\": \"male\",\n" +
                        "    \"email\": \"" + faker.internet().emailAddress() + "\",\n" +
                        "    \"status\": \"active\"\n" +
                        "}")
                .when().patch("https://gorest.co.in/public/v2/users/" + postId)
                .then().log().all().extract().response();

        int patchId = response.jsonPath().getInt("id");
        Assert.assertEquals(postId, patchId, "Expected id " + postId + " we found " + patchId);

        // Creating the get request to delete the specific user
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer 5bd0864222b09a08d5a56ac71311588b8bbd59cee33fe96027ae65783fd4e0a8")
                .when().delete("https://gorest.co.in/public/v2/users/" + postId)
                .then().log().all().extract().response();
    }}