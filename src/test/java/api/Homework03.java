package api;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;

public class Homework03 {
    public static void main(String[] args) {


        /**
         * Response is an interface coming from the RestAssured Library
         * The Response variable "response" stores all the components of API calls
         * including the request and response
         * RestAssured is written with BDD flow
         *
         */
        Response response;

        Faker faker = new Faker();

        // Creating the post request
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .body("{\n" +
                        "    \"firstName\": \"" + faker.name().firstName() + "\",\n" +
                        "    \"lastName\": \"" + faker.name().lastName() + "\",\n" +
                        "    \"email\": \"" + faker.internet().emailAddress() + "\",\n" +
                        "    \"dob\": \"1992-12-15\"\n" +
                        "}")
                .when().post("https://tech-global-training.com/users")
                .then().log().all().extract().response();


        int postId = response.jsonPath().getInt("id");

        System.out.println("Id is coming from response " + postId);

        // Creating the get request to fetch specific user
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .when().post("https://tech-global-training.com/users")
                .then().log().all().extract().response();


        // Creating the PUT request to update the existing user
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .body("{\n" +
                        "    \"firstName\": \"" + faker.name().firstName() + "\",\n" +
                        "    \"lastName\": \"" + faker.name().lastName() + "\",\n" +
                        "    \"email\": \"" + faker.internet().emailAddress() + "\",\n" +
                        "    \"dob\": \"1992-12-15\"\n" +
                        "}")
                .when().post("https://tech-global-training.com/users")
                .then().log().all().extract().response();


        // Creating the get request to update the existing user
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .body("{\n" +
                        "    \"firstName\": \"" + faker.name().firstName() + "\",\n" +
                        "    \"lastName\": \"" + faker.name().lastName() + "\",\n" +
                        "    \"email\": \"" + faker.internet().emailAddress() + "\",\n" +
                        "    \"dob\": \"1992-12-15\"\n" +
                        "}")
                .when().post("https://tech-global-training.com/users")
                .then().log().all().extract().response();

        int patchId = response.jsonPath().getInt("id");
        Assert.assertEquals(postId, patchId, "Expected id " + postId + " we found " + patchId);

        // Creating the get request to delete the specific user
        response = RestAssured
                .given().log().all()
                .header("Content-Type", "application/json")
                .when().post("https://tech-global-training.com/users")
                .then().log().all().extract().response();
    }}