@common
  Feature: As a QE, I validate data from DB, API and UI
    Scenario Outline: Validating data from DB, API and UI
      Given user is able to connect to database
      And user send "<query>" to database and result
      And I send GET request to "<urlPath>"
      And user navigates to "https://techglobal-training.netlify.app/"
      And I convert students json response to Java
      And user clicks on Practices dropdown in the header
      And user selects the "Backend Testing" option
      When user can see all students on the page
      Then I validate data between DB, API and UI

      Examples: Data
        | query                 | urlPath   |
        | select * from student | /students |