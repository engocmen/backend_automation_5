@dbapi

Feature: As a QE, I am validating the data from Database with API

  Scenario Outline: Validating the data from Database and API
    Given user is able to connect to database
    And user send "<query>" to database and result
    And I send GET request to "<urlPath>"
    When I convert students json response to Java
    Then I validate data from DB with API


    Examples: Data
      | query                 | urlPath   |
      | select * from student | /students |