######################################################
# This feature file is testing the GoRest CRUD
# Author: Tech Global
# Date: 03/21/2023
######################################################

@smoke
Feature: As I QE, I validate GoRest Post

  Scenario Outline: Validating the POST Request
    Given Create a GoRest user with "<name>", "<gender>", email, "<status>" and "<urlPath>"
    Then Status code is 201

    Examples: GoRest Data

      | name       | gender | status | urlPath           |
      | Batch Five | female | active | /public/v2/users/ |